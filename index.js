require('dotenv').config()

const express = require('express')
const app = express()
const port = process.env.PORT

const bcrypt = require('bcrypt');
const saltRounds = 10;
const username = 'walrus'
const password = 'walrus'

const logger = require('morgan')
const passport = require('passport')
const cookieParser = require('cookie-parser')
const fortune = require('fortune-teller')

const MongoClient = require('mongodb').MongoClient;
const url = process.env.MONGO_URL;

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("nsaa");
  var userHash
  userHash = bcrypt.hashSync(password, saltRounds);
  var userData = {
    username: username,
    hash: userHash
  };
  dbo.collection("users").remove({ username: username })
  dbo.collection("users").insertOne(userData, function(err, res) {
    if (err) throw err;
    console.log("user created")
  });

});

const JwtStrategy = require('passport-jwt').Strategy;

const LocalStrategy = require('passport-local').Strategy
const jwt = require('jsonwebtoken')
const jwtSecret = require('crypto').randomBytes(16)

const cookieExtractor = function(req) {
    var token = null;
    if (req && req.cookies)
    {
        token = req.cookies['auth_token'];
    }
    return token;
};

const opts = {}
opts.jwtFromRequest = cookieExtractor;
opts.secretOrKey = jwtSecret;
opts.issuer = 'localhost:3000';
opts.audience = 'localhost:3000';

app.use(cookieParser())
app.use(logger('dev'))

passport.use(new JwtStrategy(opts,
  function(jwt_payload, done) {
    return done(null,jwt_payload)
  }
));

passport.use('local', new LocalStrategy(
  {
    usernameField: 'username', // it MUST match the name of the input field for the username in the login HTML formulary
    passwordField: 'password', // it MUST match the name of the input field for the password in the login HTML formulary
    session: false // we will store a JWT in the cookie with all the required session data. Our server does not need to keep a session, it's stateless
  },
  function (username, password, done) {

    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db("nsaa");
      var userHash
      userHash = bcrypt.hashSync(password, saltRounds);
      dbo.collection("users").findOne({username:username})
      .then(data => {
        userData = data
        bcrypt.compare(password, userData.hash, function(err, result) {
          const user = {
            username: 'walrus',
            description: 'the only user that deserves to contact the fortune teller'
          }
          return done(null, user) // the first argument for done is the error, if any. In our case no error so that null. The object user will be added by the passport middleware to req.user and thus will be available there for the next middleware and/or the route handler
        // }
        // return done(null, false)
        });
      })
      db.close()
    });

  }
))

app.use(express.urlencoded({ extended: true })) // needed to retrieve html form fields
app.use(passport.initialize()) // we load the passport auth middleware to our express application. It should be loaded before any route.

app.get('/', passport.authenticate('jwt', { session: false }),
  (req, res) => {
    console.log(req.user)
    res.send(fortune.fortune())
  }
)

app.get('/login',
  (req, res) => {
    res.sendFile('login.html', { root: __dirname })
  }
)

app.post('/login',
  passport.authenticate('local', { failureRedirect: '/login', session: false }),
  (req, res) => {
    // we should create here the JWT for the fortune teller and send it to the user agent inside a cookie.
    // This is what ends up in our JWT
    const jwtClaims = {
      sub: req.user.username,
      iss: 'localhost:3000',
      aud: 'localhost:3000',
      exp: Math.floor(Date.now() / 1000) + 604800, // 1 week (7×24×60×60=604800s) from now
      role: 'user' // just to show a private JWT field
    }


    // generate a signed json web token. By default the signing algorithm is HS256 (HMAC-SHA256), i.e. we will 'sign' with a symmetric secret
    const token = jwt.sign(jwtClaims, jwtSecret)
    res.cookie('auth_token', token, { maxAge: 900000, httpOnly: true })

    // Just for testing, send the JWT directly to the browser. Later on we should send the token inside a cookie.
    res.json(token)
    // And let us log a link to the jwt.iot debugger, for easy checking/verifying:
    console.log(`Token sent. Debug at https://jwt.io/?value=${token}`)
    console.log(`Token secret (for verifying the signature): ${jwtSecret.toString('base64')}`)
  }
)

app.get('/logout', passport.authenticate('jwt', { session: false }),
  function (req, res) {
    req.logout()
    res.clearCookie('auth_token')
    res.redirect('/login');
  }
)

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
